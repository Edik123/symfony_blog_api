up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-pull docker-build docker-up blog-init

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

blog-cli:
	docker-compose run --rm blog-php-cli php bin/console

blog-init: blog-composer-install blog-migrations blog-fixtures

blog-migrations:
	docker-compose run --rm blog-php-cli php bin/console doctrine:migrations:migrate --no-interaction

blog-fixtures:
	docker-compose run --rm blog-php-cli php bin/console doctrine:fixtures:load --no-interaction

blog-composer-install:
	docker-compose run --rm blog-php-cli composer install

